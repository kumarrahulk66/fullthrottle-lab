# Django Application  #

An example Django REST framework project for an API to serve that data in the json format given above.

# Create Virtualenv
 	virtualenv -p python3 ~/env 
 Activate the env using command **source ~/env/bin/activate**


### Install 

    pip install -r requirements.txt

### Django migrations and migrate for database
	python3 manage.py makemigrations 
	python3 manage.py migrate 
  above commands for for database taple 

### How do I run custom management command ? ###

* python3 manage.py custom_command 10

By Running this command data will populate in to database with some dummy data. 

### API Endpoints

#### Users

* **/activity/list_users/** (User get data endpoint)
while hitting **http://127.0.0.1:8000/activity/list-users/** this url user will get User and ActivityPeriod Json Data as a response.


### Usage

    python3 manage.py runserver


