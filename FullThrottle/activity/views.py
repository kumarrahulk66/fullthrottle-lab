from rest_framework import serializers, status, generics
from rest_framework.response import Response
from django.contrib.auth.models import User


# Create your views here.
from activity.serializers import  StatusSerializer


class ListUsers(generics.ListAPIView):

    def list(self, request, *args, **kwargs):
        queryset = User.objects.all()
        serializer = StatusSerializer(queryset)
        return Response(serializer.data, status=status.HTTP_200_OK)

