from django.contrib.auth.models import User
from rest_framework import serializers, status, generics
from django.contrib.auth.models import User

from activity.models import ActivityPeriod


class ActivityPeriodSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActivityPeriod
        fields = ('start_time', 'end_time')


class UserDetailsSerializer(serializers.ModelSerializer):
    real_name = serializers.SerializerMethodField()
    tz = serializers.SerializerMethodField()
    activity_periods = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = [
            'id',
            'real_name',
            'tz',
            'activity_periods'
        ]

    def get_tz(self, obj):
        if obj:
            try:
                return obj.date_joined.tzinfo.zone
            except:
                return None
        return None

    def get_real_name(self, obj):
        if obj:
            return obj.first_name + ' ' + obj.last_name

    def get_activity_periods(self, obj):
        if obj:
            qs = ActivityPeriod.objects.filter(user_id=obj.id)
            return ActivityPeriodSerializer(instance=qs, many=True).data


class StatusSerializer(serializers.Serializer):
    ok = serializers.SerializerMethodField()
    members = serializers.SerializerMethodField()

    def get_ok(self, obj):
        return 'true' if obj else 'false'

    def get_members(self, obj):
        return UserDetailsSerializer(obj, many=True).data

