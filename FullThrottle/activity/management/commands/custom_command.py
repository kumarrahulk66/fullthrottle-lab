from django.core.management.base import BaseCommand
from django.utils import timezone
from faker import Faker
from activity.models import ActivityPeriod
from django.utils.crypto import get_random_string
from django.contrib.auth.models import User


class Command(BaseCommand):
    help = 'Create Dummy Data'

    def add_arguments(self, parser):
        parser.add_argument('total', type=int, help='Indicates the number of users to be created')

    def handle(self, *args, **kwargs):
        obj = Faker()
        total = kwargs['total']
        for i in range(total):
            full_name = obj.name()
            name_list = full_name.split()
            first_name = name_list[0]
            last_name = name_list[1]
            user_object = User.objects.get_or_create(username=get_random_string(),
                                                   email='', password='123',
                                                   first_name=first_name, last_name=last_name)[0]
            for _ in range(3):
                activity_object = ActivityPeriod.objects.get_or_create(
                    user=user_object, start_time=obj.date_time(), end_time=obj.date_time())[0]

        self.stdout.write("It's user %s" % user_object)
