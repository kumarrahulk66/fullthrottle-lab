from django.db import models

# Create your models here.

from django.contrib.auth.models import User


class ActivityPeriod(models.Model):
    """ActivityPeriod."""

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    start_time = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    end_time = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    # def __str__(self):
    #     """__str__."""
    #     return self.user

    class Meta:
        """Meta."""

        managed = True
        db_table = "activity_period"
        ordering = ["pk"]
