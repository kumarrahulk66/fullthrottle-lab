from django.conf.urls import url, include

from .views import ListUsers

urlpatterns = [
        url(r'list_users/', ListUsers.as_view())
]
